import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
        String artikel = liesString(myScanner);
        int amount = liesInt(myScanner);
        double preis = liesDouble(myScanner);
        double nettogesamtpreis = berechneGesamtnettopreis(amount, preis);
        System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
        double mwst = myScanner.nextDouble();
        
        // Bruttogesgesamtpreis
        double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}
	
	public static String liesString(Scanner myScanner) {
		System.out.println("was m�chten Sie bestellen?");
		return myScanner.next();
		
	}
	
	

}
