import java.util.Scanner;
public class Mittelwert {

   public static void main(String[] args) {
	   Scanner scan = new Scanner(System.in); 
	   
      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = eingabe(scan);
      double y = eingabe(scan);
      
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      double m = verarbeitung(x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      ausgabe(x, y, m);
      
   }
   	public static double eingabe(Scanner scan1) {
	System.out.println("Bitte geben Sie eine Zahl ein: ");
	double zahl = scan1.nextDouble();
   		return zahl;
   }
   	public static double verarbeitung(double x, double y) {
   	 double m;
   		m = (x + y) / 2.0;
   	 return m;
   	}
   	
   	public static void ausgabe(double x, double y, double m) {
   		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   	
   	}
}
