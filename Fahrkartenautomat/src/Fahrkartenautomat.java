﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneMünze;
       float rückgabebetrag;
       byte anzahlTickets;

       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       
       // Geldeinwurf
       // -----------
       rückgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
     
       fahrkartenAusgeben();
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
    //   rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    
    public static float fahrkartenbestellungErfassen(Scanner myScanner) {
    	System.out.print("Zu zahlender Betrag (EURO): ");
        float zuZahlenderBetrag = myScanner.nextFloat();
        
        System.out.print("Zu kaufende Tickets: ");
        byte anzahlTickets = myScanner.nextByte();
        
        zuZahlenderBetrag *= anzahlTickets; //zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
        
        return zuZahlenderBetrag;
        
    }
    public static float fahrkartenBezahlen(Scanner myScanner, float zuZahlen) {
    	 float eingezahlterGesamtbetrag = 0.0f;
         while(eingezahlterGesamtbetrag < zuZahlen) {
      	   System.out.printf("Noch zu zahlen: %.2f\n", (zuZahlen - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   float eingeworfeneMünze = myScanner.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
         }
         
         return eingezahlterGesamtbetrag - zuZahlen;
    
    }
    
    public static float fahrkartenAusgeben() {
    	  System.out.println("\nFahrschein wird ausgegeben");
          for (int i = 0; i < 8; i++)
          {
             System.out.print("=");
             try {
   			Thread.sleep(250);
   		} catch (InterruptedException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
          }
          System.out.println("\n\n");	
    	
          
    }

    }