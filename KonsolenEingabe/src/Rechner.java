	import java.util.Scanner; // Import der Klasse Scanner
		
		public class Rechner
		{
		public static void main(String[] args) // Hier startet das Programm
		{
		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		// Die Variable zahl1 speichert die erste Eingabe
		int zahl1 = myScanner.nextInt();
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		// Die Variable zahl2 speichert die zweite Eingabe
		int zahl2 = myScanner.nextInt();
		System.out.print("Bitte geben Sie eine dritte ganze Zahl ein: ");
		int zahl3 = myScanner.nextInt();
		System.out.print("Bitte geben Sie eine vierte ganze Zahl ein: ");
		int zahl4 = myScanner.nextInt();
		
		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		int ergebnis = zahl1 + zahl2;
		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
		myScanner.close();
		
		int ergebnisMinus = zahl1 - zahl2;
		System.out.print("\n\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisMinus);
		myScanner.close();
		
		int ergebnisMal = zahl1 * zahl2;
		System.out.print("\n\nErgebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMal);
		myScanner.close();
		
		float ergebnisDiv = zahl1 / (float)zahl2;
		System.out.print("\n\nErgebnis der Division lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDiv);
		myScanner.close();

		
		float ergebnisR = zahl1 * zahl2 - zahl3 / zahl4;
		System.out.println("\n\nErgebnis der Random lautet: ");
		System.out.println(zahl1 + " * " + zahl2 + " - " + zahl3 + " / " + zahl4 + " = " + ergebnisR);
		myScanner.close();
		
		
		
		
		
	}

}
