
public class ErsteKlasse {

	public static void main(String[] args) {

		System.out.println("Aufgabe 1\n");
		System.out.print("Die W�rde des Menschen ist unantastbar. ");
		System.out.print("Sie zu achten und zu sch�tzen ist Verpflichtung aller staatlichen Gewalt.\n\n");

		String h1 = "Menschen";
		String h2 = "Verpflichtung";

		System.out.println("Die W�rde des " + h1 + " ist \"unantastbar\".");
		System.out.println("Sie zu achten und zu sch�tzen ist " + h2 + " aller staatlichen Gewalt.");

		// Der Unterschied zwischen print() und println besteht darin, dass println()
		// zus�tzlich einen Zeilenumbruch nach der Ausgabe einf�gt. Man braucht keine
		// zus�tzliche wie bei print() \n einzugeben.

		System.out.println(" ");
		System.out.println("Aufgabe 2\n");

		String f = "*************";

		System.out.printf("%7.1s\n", f);
		System.out.printf("%8.3s\n", f);
		System.out.printf("%9.5s\n", f);
		System.out.printf("%10.7s\n", f);
		System.out.printf("%11.9s\n", f);
		System.out.printf("%12.11s\n", f);
		System.out.printf("%13.13s\n", f);
		System.out.printf("%8.3s\n", f);
		System.out.printf("%8.3s\n", f);

		System.out.println(" ");
		System.out.println("Aufgabe 3\n");

		double a1 = 22.4234234;
		double a2 = 111.2222;
		double a3 = 4.0;
		double a4 = 1000000.551;
		double a5 = 97.34;

		System.out.printf("%.2f\n%.2f\n%.2f\n%.2f\n%.2f\n", a1, a2, a3, a4, a5);

	}

}
