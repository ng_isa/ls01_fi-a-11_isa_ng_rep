import java.util.Locale;


public class Konsolenausgabe2 {

	public static void main(String[] args) {
		System.out.println("Aufgabe 1\n");
		String f = "**";

		System.out.printf("%5.2s\n", f);
		System.out.printf("%1.1s", f);
		System.out.printf("%7.1s\n", f);
		System.out.printf("%1.1s", f);
		System.out.printf("%7.1s\n", f);
		System.out.printf("%5.2s\n", f);

		System.out.println("Aufgabe 2\n");

		System.out.printf("%-5s = %-18s = %4s\n", "0!", "", "1");
		System.out.printf("%-5s = %-18s = %4s\n", "1!", 1, 1);
		System.out.printf("%-5s = %-18s = %4s\n", "2!", "1 * 2", 1 * 2);
		System.out.printf("%-5s = %-18s = %4s\n", "3!", "1 * 2 * 3", 100/5);
		System.out.printf("%-5s = %-18s = %4s\n", "4!", "1 * 2 * 3 * 4", 1 * 2 * 3 * 4);
		System.out.printf("%-5s = %-18s = %4s\n\n", "5!", "1 * 2 * 3 * 4 * 5", 1 * 2 * 3 * 4 * 5);
		
		System.out.println("Aufgabe 3\n");

		String f0 = "Fahrenheit";
		String c0 = "Celsius";
		String line = "--------------------------";

		int f1 = 20;
		int f2 = 10;
		int f3 = 0;
		int f4 = 30;

		double c1 = -28.8889;
		double c2 = -23.3333;
		double c3 = -17.7778;
		double c4 = -6.6667;
		double c5 = -1.1111;

		System.out.printf("%-12s|   %10s\n", f0, c0);
		System.out.printf("%s\n", line);
		System.out.printf(Locale.US, "%-12s|   %10.2f\n", -f1, c1);
		System.out.printf(Locale.US, "%-12s|   %10.2f\n", -f2, c2);
		System.out.printf(Locale.US, "+" + "%-11s|   %10.2f\n", +f3, c3);
		System.out.printf(Locale.US, "+" + "%-11s|   %10.2f\n", +f1, c4);
		System.out.printf(Locale.US, "+" + "%-11s|   %10.2f\n", +f4, c5);

		// System.out.printf( "%.0f |%.2f\n%.0f |%.2f\n%+.0f |%.2f\n%+.0f |%.2f\n%+.0f
		// |%.2f\n" , -f1, c1, -f2, c2, +f3, c3, +f1, c4, +f4, c5);
		// System.out.printf("%.2f\n%.2f\n%.2f\n%.2f\n%.2f\n", c1,c2,c3,c4,c5);
		
		System.out.println("Aufgaben test\n\n");

		System.out.printf("%-5s = %-19s = %4s\n", "01!", " ", "1");
		System.out.printf("%-5s = %-19s = %4s\n", "02!", "1", "1");
		System.out.printf("%-5s = %-19s = %4s\n", "03!", "1 * 2", "2");
		System.out.printf("%-5s = %-19s = %4s\n", "04!", "1 * 2 * 3 ", "6");
		System.out.printf("%-5s = %-19s = %4s\n", "05!", "1 * 2 * 3 * 4", "24");
		System.out.printf("%-5s = %-19s = %4s\n\n", "06!", "1 * 2 * 3 * 4 * 5", 120);
		
		
		String x = "Hello World";
		System.out.printf("%-4s = %-18s\n", "1st", x);
		System.out.printf("%-4s = %-18s\n", "2st", x);
		System.out.printf("%-4s = %-18s\n", "3rd", x);
		System.out.printf("%-7s! = %-18s\n", "4th", x);
		System.out.printf("%-10s! = %-18s\n\n", "5th", x);
		System.out.printf("%S\n", x);
		
		
		
		String a0 = "Fahrenheit", a1 = "Celsius";
		int a3 = -20, a4 = -10, a5 = +0, a6 = 20, a7 = 30;
		double a8 = -28.8889, a9 = -23.3333, a10 = -17.7778, a11 = -6.6667, a12 = -1.1111;
		
		
		System.out.printf("%-12s| %10s\n", a0, a1);
		System.out.println("------------------------");
		System.out.printf(Locale.US, "%-12s| %10.2f\n", a3, a8);
		System.out.printf(Locale.US, "%-12s| %10.2f\n", a4, a9);
		System.out.printf(Locale.US, "+" + "%-11s| %10.2f\n", a5, a10);
		System.out.printf(Locale.US, "+" + "%-11s| %10.2f\n", a6, a11);
		System.out.printf(Locale.US, "+" + "%-11s| %10.2f\n", a7, a12);
		
		System.out.printf("X = %.3f %nY = %.3f %nZ = %.2f", a8, a9, a10);

		System.out.printf("Hello %S!%n", "World");
		System.out.printf("%S%n'%s'%n", "example1", "example2");
		System.out.printf("|%-15S|%n", "example3");
		System.out.printf("|%15S|%n", "example4");
		System.out.printf("%.7s%n", "Hi example5");
		System.out.printf("simple integer: %d%n", 10000L);
		System.out.printf("simple integer: %d%n", 10000);
		System.out.printf("simple integer: %d%n", 50000L);
		System.out.printf(Locale.US, "%,d %n", 10000000);
		System.out.printf(Locale.GERMANY, "%,d %n", 10000000);
		System.out.printf("&&&%-5.3f%n", 52.1479);
		System.out.printf("%f%n", 5.1473);
		System.out.printf("'%-20.2fMerry Christmas%n",  5.1473);
		System.out.printf("'%.2f'%n%n", 5.1473);

		System.out.printf("%b%n", true);
		System.out.printf("%B%n", false);
		System.out.printf("%B%n", 5.3);
		System.out.printf("%b%n", "random text");
		System.out.printf("%b%n", "hello world");
		System.out.printf("wir%nschaffen%ndas%n");
		
	
	
		

		//System.out.printf("hours %tH: minutes %tM: seconds %tS%n", date, date, date);
		
		
		

	
		
	}

}
