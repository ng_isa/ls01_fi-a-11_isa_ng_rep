
public class TestArray {

	public static void main(String[] args) {
		
		
		int [] num = {0,1,2,3,9,10,11};
		
		//Speicherplatz von num (in diesen Computer) anzeigen
		System.out.println(num);
		
		//Inhalt an Index 0 anzeigen
		System.out.println(num[0]);
		
		//gesamter Inhalt anzeigen
		for (int i = 0; i < num.length; i++) {
			
			System.out.println("Index " + num[i] + ": " + num[i]);
			
			}
		// gesamtes Array füllen - überschreiben
		for (int i = 0; i < num.length; i++) {
			//num[i] = 6;
			System.out.println(num[i] + 15);
			
		}
		
	}

}
