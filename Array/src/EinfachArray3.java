import java.util.Scanner;

public class EinfachArray3 {

	public static void main(String[] args) {

		char Eingabe[] = new char[5];
		Scanner sc = new Scanner(System.in);

		for (int i = 0; i < Eingabe.length; i++) {
			Eingabe[i] = sc.next().charAt(0);

		}

		for (int i = Eingabe.length - 1; i >= 0; i--) {
			System.out.println(Eingabe[i]);

		}

	}

}
