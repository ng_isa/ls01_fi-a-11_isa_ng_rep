public class BspArray {

	public static void main(String[] args) {
	
		int zahl [] = {2, 23, 45, 67, 19, 88};  //Schnellschreibweise
		int zahl2 [] = {2, 23, 45, 67, 19, 88};
		
		int zahl3 [] = zahl;
		
		System.out.println(zahl);
		System.out.println(zahl2);
		System.out.println(zahl3 == zahl);
		System.out.println("---------------");
		System.out.println(zahl[4]);
		System.out.println(zahl.length);
		System.out.println("---------------");
		String [] worter = new String [10]; //die L�nge wird jetzt schon festgelegt werden und nicht nachtr�glich �ndern
		
		System.out.println(worter.length);
		System.out.println(worter[0]);
	}

}
