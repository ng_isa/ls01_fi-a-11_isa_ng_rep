/*Aufgabe 2
Das zu schreibende Programm �UngeradeZahlen� ist �hnlich der Aufgabe 1. Sie deklarieren
wiederum ein Array mit 10 Ganzzahlen. Danach f�llen Sie es mit den ungeraden Zahlen von 1 bis 19
und geben den Inhalt des Arrays �ber die Konsole aus (Verwenden Sie Schleifen!).  
 */
public class ArrayAufgabe2 {

	public static void main(String[] args) {
		
		int[] zahlen = new int[10];
		
//		int[] zahlen = {1,3,5,7,9,11,13,15,17,19};
		
		int add = 1;
		for(int i = 0; i <zahlen.length; i++) {
			zahlen[i] = add;
			add = add + 2;
		}
		System.out.println("Mit der foreach-Schleife");
		for(int tmp : zahlen) {
			System.out.println(tmp);
			
		}
		
		System.out.println("Mit der Z�hlschleife");
		for(int i = 0; i <zahlen.length; i++) {
			System.out.println(zahlen[i]);
			
			
		}

	}

}
