/*Aufgabe 1
Schreiben Sie ein Programm �Zahlen�, in welchem ein ganzzahliges Array der L�nge 10
deklariert wird. Anschlie�end wird das Array mittels Schleife mit den Zahlen von 0 bis 9
gef�llt. Zum Schluss geben Sie die Elemente des Arrays wiederum mit einer Schleife auf der
Konsole aus. */
public class ArrayAufgabe1 {

    public static void main(String[] args) {

        int[] zahlen = new int[20]; // Array ist der l�nge 10
        //hier die Eckige klammer nicht vergessen

        for(int i= 0; i < zahlen.length; i++) {
            System.out.println("Index " + i +" : "+ zahlen[i] + " | ");
        }


        for(int i= 0; i < zahlen.length; i++) {
            zahlen[i] = i+1;
        }

        System.out.println("\nArray danach: ");

        for(int i= 0; i < zahlen.length; i++) {
            System.out.print("Index " + i +" : "+ zahlen[i] + " | ");
        }



    }

}