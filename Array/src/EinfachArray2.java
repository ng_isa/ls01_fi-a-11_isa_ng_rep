public class EinfachArray2 {

	public static void main(String[] args) {

		int zahlen[] = new int[10];
		int ungeradeZahl = 1;

		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = ungeradeZahl;
			ungeradeZahl = ungeradeZahl + 2;

		}

		for (int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);

		}

	}

}
