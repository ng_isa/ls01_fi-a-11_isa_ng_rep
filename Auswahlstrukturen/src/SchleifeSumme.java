import java.util.Scanner;

//Geben Sie in der Konsole die Summe der Zahlenfolgen aus. 
//Ermöglichen Sie es dem Benutzer die Zahl n festzulegen, welche die Summierung begrenzt.
//a) 1 + 2 + 3 + 4 +…+ n for-Schleife / while-Schleife
//b) 2 + 4 + 6 + 8 + 10 …+ 2n for-Schleife / while-Schleife
//c) 1 + 3 + 5 +…+ (2n+1) for-Schleife / while-Schleife

public class SchleifeSumme {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.print("Geben sie die letzte Zahl ein: ");
		int n = scanner.nextInt();
		
		int summe = 0;
		for (int i = 1; i <= n; i++) {
			summe += i;

			if (i != n) {
				System.out.print(i + " + ");
			} else {
				System.out.print(i + " = " + summe);
			}

		}

	}

}
