import java.util.Scanner;
public class Schleifen {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//Eine r�mische Zahl zu Dezimalzahl
		//Man ben�tigt switch-case (wie bei einem Men�)
		
		char wahl = 'J';
		
		do {
			
		System.out.print("Bitte Zeichen eingeben: ");
		wahl = sc.next().toUpperCase().charAt(0); //r�mische Zahl wird eingelesen und grunds�tzlich gro�geschrieben
		umwandeln(wahl);
		System.out.println("M�chten Sie wiederholen? (J: wiederholen, N: stoppen)");
		wahl = sc.next().toUpperCase().charAt(0);
		
		}while(wahl == 'J'); //fu�gesteuerte Schleife. L�uft mind. einmal
		
		//Variante kopfgesteuert, l�uft solange Bedingung erf�llt ist d.h. m�glicherweise auch keinmal
		
	}

		public static int umwandeln(char wahl) {
		int dezzahl = 0;
		
		switch (wahl) {
		case 'I':
			dezzahl = 1;
			System.out.println(dezzahl);
			break;
			
			case 'V':
				dezzahl = 5;
				System.out.println("5");
				break;
				
			case 'X':
				dezzahl = 10;
				System.out.println("10");
				break;
				
			case 'C':
				dezzahl = 100;
				System.out.println("100");
				break;
				
		
			case 'D':
				dezzahl = 500;
				System.out.println("500");
				break;
				
			case 'M':
				dezzahl = 1000;
				System.out.println("1000");
				break;
				
			default:
				System.out.println("Zeichen gibt es nicht");
			
		}	
		
	return dezzahl;
}
}
