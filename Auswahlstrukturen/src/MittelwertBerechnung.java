import java.util.Scanner;

public class MittelwertBerechnung {

	public static void main(String[] args) {
		double zahl, summe = 0;
		double m;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		System.out.println("Wie viele Zahlen wollen Sie eingeben? ");
		int anzahl = myScanner.nextInt();

		for (int i = 1; i <= anzahl; i++) {
			zahl = eingabe(myScanner, "Bitte geben Sie eine Zahl ein: ");
			summe = summe + zahl;
		}
		m = mittelwertBerechnung(summe, anzahl);

		ausgabe(m);

		myScanner.close();
	}

	public static double eingabe(Scanner ms, String text) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}

	public static double mittelwertBerechnung(double summe, int anzahl) {
		double m = summe / anzahl;
		return m;
	}

	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert aller Zahlen ist: " + mittelwert);
	}
}