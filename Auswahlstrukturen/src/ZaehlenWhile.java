//Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend 
//(bzw. von n bis 1 herunterz�hlend) aus. 
//Erm�glichen Sie es dem Benutzer die Zahl n festzulegen. 
//Nutzen Sie zur Umsetzung eine while-Schleife.
//a) 1,2,3, ..., n
//b) n, ..., 3,2,1

public class ZaehlenWhile {

	public static void main(String[] args) {

		int zahl = 0;
		int zahl2 = 10;

		while (zahl < 10) {
			zahl++;

			if (zahl < 10)
				System.out.print(zahl + " , ");
			else
				System.out.print(zahl);

		}
		System.out.println("");

		while (zahl2 > 0) {
			zahl2--;

			if (zahl2 > 0)
				System.out.print(zahl2 + " , ");
			else
				System.out.println(zahl2);

		}

	}

}
