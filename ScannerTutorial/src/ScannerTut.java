import java.util.Scanner;

public class ScannerTut {

	public static void main(String[] args) {
		// create User Input with a Scanner
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("What is your full name? ");
		//String name = scan.next();
	//	name = name + " " + scan.next();
		String name = "";
		name = name + scan.nextLine();
		
		// store the name input by User by using the String function above
		// System.out.print(name);
		
		System.out.print("What is your age? ");
		// scan.nextInt();
		int age = scan.nextInt();
		
		System.out.print("What is your favourite quote? ");
		String quote = scan.next();

		//quote = quote + scan.nextLine();
		quote += scan.nextLine();

		System.out.println("Thank you very much " + name + ", you are " + age + " years old. Your favourite quote is " + "\" "  +  quote  + " \""  + ".");
		
		System.out.print("Where do you live? ");
		String city = scan.next();
		
		System.out.print("What is your hobby? ");
		String hobby = scan.next();
		hobby += scan.nextLine();
		
		System.out.println(city + " is a nice place to live for people who are " + age + " years old and love " + hobby + ".");
		
		System.out.print("What is your mobile phone number? ");
		int phoneNumber = scan.nextInt();
		
		
		System.out.println(name + "!! " + "You should not give away your private number so casually.");
	
		
		
	}

}
